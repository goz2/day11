import {useState} from 'react'
import {useDispatch} from 'react-redux'
import {generateTodo} from '../../slices/listSlice'
import './index.css'
function TodoGenetator() {
    const [text, setText] = useState('')
    const dispatch = useDispatch()
    const handleAdd = () => {
        if (text && text.trim().length > 0) {
            dispatch(generateTodo({text: text, done: false}))
            setText('')
        }
    }
    return (
        <div>
            <input className="marginSplit" type="text" onChange={e => setText(e.target.value)} value={text}/>
            <input type="submit" value="ADD" onClick={handleAdd}/>
        </div>
    )
}

export default TodoGenetator