## ORID homework


	O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
	R (Reflective): Please use one word to express your feelings about today's class.
	I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
	D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?


- O (Objective): Learned about Redux, rewriting calculators with Redux, rewriting to-do lists with Redux, and front-end automated testing. We engaged in activities that involved understanding the principles of Redux, implementing Redux in calculators and to-do lists.

- R (Reflective): Enthusiastic.

- I (Interpretive): Today's class was quite informative and practical. Learning about Redux was beneficial as it's a popular state management library used in modern web development. Rewriting calculators and to-do lists with Redux provided hands-on experience, showcasing how Redux simplifies state management and improves application scalability. 

- D (Decisional): I am excited to apply what I've learned today in my personal projects and future professional endeavors. I see great potential in utilizing Redux to enhance the state management of my web applications and improve their overall performance. 