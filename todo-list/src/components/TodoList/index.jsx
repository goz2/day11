import TodoGenetator from '../TodoGenerator'
import TodoItemGroup from '../TodoItemGroup'
import './index.css'
function TodoList() {
    return (
        <div>
            <div className="headerMargin">Todo List</div>
            <TodoItemGroup/>
            <TodoGenetator/>
        </div>
    )
}

export default TodoList