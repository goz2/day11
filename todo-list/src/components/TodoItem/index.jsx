import {useDispatch} from 'react-redux'
import {removeTodo, revertTodo} from '../../slices/listSlice'
import './index.css'

function TodoItem({value}) {
    const dispatch = useDispatch()
    return (
        <div className="mainItem">
            <div>
                <span className={!value.done ? 'normal' : 'line'} onClick={() => dispatch(revertTodo(value.id))}>
                    {value.text}
                </span>
                
                <span className="button">
                    <input value="X" type="submit" onClick={() => dispatch(removeTodo(value.id))}/>
                </span>
            </div>

        </div>
    )
}

export default TodoItem