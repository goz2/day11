import {configureStore} from '@reduxjs/toolkit'
import listSlice from '../slices/listSlice'

export default configureStore({
    reducer: {
        todo: listSlice
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        serializableCheck: false
    }),
})