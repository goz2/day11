import {useSelector} from 'react-redux'
import TodoItem from '../TodoItem'

function TodoGroup() {
    const todoList = useSelector(state => state.todo.todoList)
    return (
        <div>
            {
                todoList.map(todo => <TodoItem key={todo.id} value={todo}/>)
            }
        </div>
    )
}

export default TodoGroup