import {createSlice} from '@reduxjs/toolkit'

function Item(id, text, done) {
    this.id = id
    this.text = text
    this.done = done
}

const genetateMax = (list) => {
    let max = 0
    list.forEach(cur => {
        if (cur.id > max) {
            max = cur.id
        }
    })
    return max + 1
}
export const counterSlice = createSlice({
    name: 'todoList',
    initialState: {
        todoList: [],
    },
    reducers: {
        generateTodo: (state, action) => {
            state.todoList.push(new Item(genetateMax(state.todoList), action.payload.text, action.payload.done))
        },
        removeTodo: (state, action) => {
            state.todoList = state.todoList.filter(todo => Number(todo.id) !== Number(action.payload))
        },
        revertTodo: (state, action) => {
            const tempList = [...state.todoList]
            for (let i = 0; i < tempList.length; i++) {
                if (Number(tempList[i].id) === Number(action.payload)) {
                    tempList[i].done = !tempList[i].done
                }
            }
            state.todoList = tempList
        },
    },
})

export const {generateTodo, removeTodo, revertTodo} = counterSlice.actions

export default counterSlice.reducer